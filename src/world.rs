use super::rendering::{render_bloated, render_blob, render_curve, render_filled, render_line};

use femtovg::{Canvas, Renderer};
use game_core::world::{World, shapes::ShapeKind};
use vector_basis::Basis;
use vector_space::InnerSpace;

pub fn render_world<V: InnerSpace + Basis<0> + Basis<1>, R: Renderer>(
    world: &World<V>,
    canvas: &mut Canvas<R>,
) where
    V::Scalar: Into<f32> + From<f32>,
{
    for shape_group in &world.shape_groups {
        for values in shape_group.objects.iter().flatten() {
            for shape in &shape_group.shapes {
                let shape_data = shape.eval(values);
                use ShapeKind::*;
                match shape_data.kind {
                    Line => render_line(shape_data.points, shape_data.line, canvas),
                    Curve => render_curve(shape_data.points, shape_data.line, canvas),
                    ShapeKind::Filled(color) => {
                        render_filled(shape_data.points, shape_data.line, color, canvas)
                    }
                    ShapeKind::Blob(color) => {
                        render_blob(shape_data.points, shape_data.line, color, canvas)
                    }
                    ShapeKind::Bloated { rad, color } => {
                        render_bloated(shape_data.points, shape_data.line, rad, color, canvas)
                    }
                }
            }
        }
    }
}
