use crate::convert::Convert as _;

use femtovg::{Canvas, Paint, Path, Renderer};
use game_core::world::shapes::LineData;
use num_traits::Zero;
use simple_color::Color;
use vector_basis::{Bases as _, Basis};
use vector_space::InnerSpace;

pub fn render_filled<V, R: Renderer>(
    points: Vec<V>,
    line: LineData<V::Scalar, Color>,
    fill_color: Color,
    canvas: &mut Canvas<R>,
) where
    V: InnerSpace + Basis<0> + Basis<1>,
    V::Scalar: Into<f32>,
{
    let count = points.len();

    let mut path = Path::new();

    let start = points[0];
    let sx = start.bases::<0>().into();
    let sy = start.bases::<1>().into();
    path.move_to(sx, sy);

    if count == 1 {
        path.line_to(sx, sy);
    } else if count == 2 {
        let end = points[1];
        let ex = end.bases::<0>().into();
        let ey = end.bases::<1>().into();
        path.line_to(ex, ey);
    } else {
        for next in &points[1..] {
            let nx = next.bases::<0>().into();
            let ny = next.bases::<1>().into();
            path.line_to(nx, ny);
        }
        path.close();
    }

    if line.width > V::Scalar::zero() {
        let mut stroke_paint = Paint::color(line.color.convert());
        stroke_paint.set_line_width(line.width.into());
        canvas.stroke_path(&path, &stroke_paint);
    }

    let fill_paint = Paint::color(fill_color.convert());
    canvas.fill_path(&path, &fill_paint);
}
