use crate::convert::Convert as _;

use femtovg::{Canvas, Paint, Path, Renderer, Solidity};
use game_core::world::shapes::LineData;
use num_traits::Zero;
use simple_color::Color;
use vector_basis::{Bases as _, Basis};
use vector_space::InnerSpace;

fn angle<V: InnerSpace + Basis<0> + Basis<1>>(from: V, to: V) -> V::Scalar {
    let fx = from.bases::<0>();
    let tx = to.bases::<0>();

    let angle = (to - from).angle(V::unit_bases::<1>());
    if fx < tx { -angle } else { angle }
}

pub fn render_bloated<V, R: Renderer>(
    points: Vec<V>,
    line: LineData<V::Scalar, Color>,
    rad: V::Scalar,
    fill_color: Color,
    canvas: &mut Canvas<R>,
) where
    V: InnerSpace + Basis<0> + Basis<1>,
    V::Scalar: Into<f32>,
{
    let count = points.len();

    let rad = rad.into();

    let start = points[0];

    let mut path = Path::new();

    if count == 1 {
        let x = start.bases::<0>().into();
        let y = start.bases::<1>().into();

        path.circle(x, y, rad);
    } else if count == 2 {
        let end = points[1];

        let sx = start.bases::<0>().into();
        let sy = start.bases::<1>().into();
        let ex = end.bases::<0>().into();
        let ey = end.bases::<1>().into();
        let angle0 = angle(start, end).into();
        let angle1 = std::f32::consts::PI + angle0;

        path.arc(sx, sy, rad, angle0, angle1, Solidity::Solid);
        path.arc(ex, ey, rad, angle1, angle0, Solidity::Solid);
        path.close();
    } else {
        // TODO: improve performance and handle corner cases correctly
        for i in 0..count {
            let before = points[i];
            let current = points[(i + 1) % count];
            let next = points[(i + 2) % count];

            let cx = current.bases::<0>().into();
            let cy = current.bases::<1>().into();

            let angle0 = angle(current, before).into();
            let angle1 = angle(next, current).into();

            path.arc(cx, cy, rad, angle0, angle1, Solidity::Solid);
        }
    }

    if line.width > V::Scalar::zero() {
        let mut stroke_paint = Paint::color(line.color.convert());
        stroke_paint.set_line_width(line.width.into() * rad);
        canvas.stroke_path(&path, &stroke_paint);
    }

    let fill_paint = Paint::color(fill_color.convert());
    canvas.fill_path(&path, &fill_paint);
}
