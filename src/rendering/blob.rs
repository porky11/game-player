use crate::convert::Convert as _;

use femtovg::{Canvas, LineCap, Paint, Path, Renderer};
use game_core::world::shapes::LineData;
use simple_color::Color;
use vector_basis::{Bases as _, Basis};
use vector_space::InnerSpace;

pub fn render_blob<V, R: Renderer>(
    points: Vec<V>,
    line: LineData<V::Scalar, Color>,
    fill_color: Color,
    canvas: &mut Canvas<R>,
) where
    V: InnerSpace + Basis<0> + Basis<1>,
    V::Scalar: Into<f32>,
{
    let count = points.len();

    let mut path = Path::new();

    let start_pos = points[0];
    let sx = start_pos.bases::<0>().into();
    let sy = start_pos.bases::<1>().into();

    let next_pos = points[1];
    let mut nx = next_pos.bases::<0>().into();
    let mut ny = next_pos.bases::<1>().into();

    let mx0 = (sx + nx) / 2.0;
    let my0 = (sy + ny) / 2.0;

    let mut mx = mx0;
    let mut my = my0;

    path.move_to(mx, my);

    for &end_pos in &points[2..count] {
        let ex = end_pos.bases::<0>().into();
        let ey = end_pos.bases::<1>().into();

        mx = (nx + ex) / 2.0;
        my = (ny + ey) / 2.0;

        path.quad_to(nx, ny, mx, my);

        nx = ex;
        ny = ey;
    }

    mx = (nx + sx) / 2.0;
    my = (ny + sy) / 2.0;

    path.quad_to(nx, ny, mx, my);
    path.quad_to(sx, sy, mx0, my0);

    canvas.fill_path(&path, &Paint::color(fill_color.convert()));

    let mut paint = Paint::color(line.color.convert());
    paint.set_line_width(line.width.into());
    paint.set_line_cap(LineCap::Round);

    canvas.stroke_path(&path, &paint);
}
