use crate::convert::Convert as _;

use femtovg::{Canvas, LineCap, Paint, Path, Renderer};
use game_core::world::shapes::LineData;
use simple_color::Color;
use vector_basis::{Bases as _, Basis};
use vector_space::InnerSpace;

pub fn render_curve<V, R: Renderer>(
    points: Vec<V>,
    line: LineData<V::Scalar, Color>,
    canvas: &mut Canvas<R>,
) where
    V: InnerSpace + Basis<0> + Basis<1>,
    V::Scalar: Into<f32>,
{
    let count = points.len();

    let mut path = Path::new();

    let start_pos = points[0];
    let sx = start_pos.bases::<0>().into();
    let sy = start_pos.bases::<1>().into();

    path.move_to(sx, sy);

    match count {
        0 => unreachable!(),
        1 => path.line_to(sx, sy),
        2 => {
            let end_pos = points[1];
            let ex = end_pos.bases::<0>().into();
            let ey = end_pos.bases::<1>().into();

            path.line_to(ex, ey);
        }
        3 => {
            let mid_pos = points[1];
            let mx = mid_pos.bases::<0>().into();
            let my = mid_pos.bases::<1>().into();

            let end_pos = points[2];
            let ex = end_pos.bases::<0>().into();
            let ey = end_pos.bases::<1>().into();

            path.quad_to(mx, my, ex, ey);
        }
        4 => {
            let mid_pos1 = points[1];
            let mx1 = mid_pos1.bases::<0>().into();
            let my1 = mid_pos1.bases::<1>().into();

            let mid_pos2 = points[2];
            let mx2 = mid_pos2.bases::<0>().into();
            let my2 = mid_pos2.bases::<1>().into();

            let end_pos = points[3];
            let ex = end_pos.bases::<0>().into();
            let ey = end_pos.bases::<1>().into();

            path.bezier_to(mx1, my1, mx2, my2, ex, ey);
        }
        _ => {
            let mid_pos = points[1];
            let mut sx = mid_pos.bases::<0>().into();
            let mut sy = mid_pos.bases::<1>().into();

            for &end_pos in &points[2..(count - 1)] {
                let ex = end_pos.bases::<0>().into();
                let ey = end_pos.bases::<1>().into();

                let mx = (sx + ex) / 2.0;
                let my = (sy + ey) / 2.0;

                path.quad_to(sx, sy, mx, my);

                sx = ex;
                sy = ey;
            }

            let end_pos = points.last().unwrap();
            let ex = end_pos.bases::<0>().into();
            let ey = end_pos.bases::<1>().into();

            path.quad_to(sx, sy, ex, ey);
        }
    }

    let mut paint = Paint::color(line.color.convert());
    paint.set_line_width(line.width.into());
    paint.set_line_cap(LineCap::Round);

    canvas.stroke_path(&path, &paint);
}
