pub struct View {
    pub y: f32,
    pub x: f32,
    pub zoom: f32,
}

impl View {
    pub fn new(zoom: f32) -> Self {
        Self {
            x: 0.0,
            y: 0.0,
            zoom,
        }
    }

    pub fn lerp_into(&mut self, other: Self, factor: f32) {
        self.x = self.x * (1.0 - factor) + other.x * factor;
        self.y = self.y * (1.0 - factor) + other.y * factor;
        self.zoom = self.zoom * (1.0 - factor) + other.zoom * factor;
    }
}
