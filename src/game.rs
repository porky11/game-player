use crate::world::render_world;

use super::{convert::Convert as _, view::View};

use communicator::Communicator;
use femtovg::{Canvas, Paint, Path, Renderer};
use game_core::{
    input::{InputData, InputMessage, Request},
    utilities::timed_friction,
    world::{World, WorldMessage},
};
use gapp::{Render, Update};
use gapp_winit::WindowInput;
use gilrs::{Axis, Button, Event, EventType, Gilrs};
use vector_basis::{Bases as _, Basis};
use vector_space::{InnerSpace, VectorSpace};
use winit::{
    event::{
        ElementState, KeyboardInput, ModifiersState, MouseButton, Touch, TouchPhase,
        VirtualKeyCode, WindowEvent,
    },
    event_loop::ControlFlow,
};

pub struct InputSettings {
    pub mouse: usize,
    pub left: usize,
    pub right: usize,
}

struct LocalInput<V> {
    mouse: InputData<V>,
    left: InputData<V>,
    right: InputData<V>,
}

impl InputSettings {
    fn generate_data<V: VectorSpace>(&self) -> LocalInput<V> {
        LocalInput {
            mouse: InputData::new(self.mouse),
            left: InputData::new(self.left),
            right: InputData::new(self.right),
        }
    }
}

pub struct TouchInfo {
    first: Option<u64>,
    second: Option<u64>,
}

pub struct Game<V: InnerSpace, C> {
    initialized: bool,
    world: World<V>,
    slowdown: f32,
    paused: bool,
    size: (f32, f32),
    zoom: f32,
    follow_view: f32,
    view: View,
    gilrs: Gilrs,
    input_data: LocalInput<V>,
    touch: TouchInfo,
    communicator: C,
}

impl<V: InnerSpace + Basis<0> + Basis<1>, C: Communicator<InputMessage<V>, WorldMessage<V>>>
    Game<V, C>
where
    V::Scalar: From<f32> + Into<f32>,
{
    pub fn new(communicator: C, input_settings: InputSettings) -> Self {
        let zoom = 0x20 as f32;
        let input_data = input_settings.generate_data();
        Self {
            initialized: false,
            world: World::empty(),
            slowdown: 0.0,
            paused: false,
            size: (0.0, 0.0),
            zoom,
            follow_view: 4.0,
            view: View::new(zoom),
            gilrs: Gilrs::new().unwrap(),
            input_data,
            touch: TouchInfo {
                first: None,
                second: None,
            },
            communicator,
        }
    }

    pub fn reload_world(&mut self, world: World<V>) {
        self.world = world;
        self.slowdown = 0.0;
        self.view.x = 0.0;
        self.view.y = 0.0;
        self.view.zoom = self.zoom;

        self.update_view(1.0);
    }

    fn update_view(&mut self, ratio: f32) {
        let mut range = None;
        for object in self.world.shape_groups[0].objects.iter().flatten() {
            for point in &object.points {
                let x = point.bases::<0>().into();
                let y = point.bases::<1>().into();
                let mut min_x = x - self.zoom / 2.0;
                let mut min_y = y - self.zoom / 2.0;
                let mut max_x = x + self.zoom / 2.0;
                let mut max_y = y + self.zoom / 2.0;
                if let Some((min_x0, min_y0, max_x0, max_y0)) = range {
                    if min_x0 < min_x {
                        min_x = min_x0
                    }
                    if min_y0 < min_y {
                        min_y = min_y0
                    }
                    if max_x0 > max_x {
                        max_x = max_x0
                    }
                    if max_y0 > max_y {
                        max_y = max_y0
                    }
                }

                range = Some((min_x, min_y, max_x, max_y));
            }
        }

        let view = if let Some((min_x, min_y, max_x, max_y)) = range {
            let min_w = max_x - min_x;
            let min_h = max_y - min_y;

            View {
                x: (min_x + max_x) / 2.0,
                y: (min_y + max_y) / 2.0,
                zoom: min_w.max(min_h),
            }
        } else {
            View::new(self.zoom)
        };
        self.view.lerp_into(view, ratio);
    }

    fn handle_gamepad_input(&mut self, event: EventType) {
        use EventType::*;
        match event {
            AxisChanged(axis, value, _) => match axis {
                Axis::LeftStickX => self.input_data.left.set_axis::<0>(value.into()),
                Axis::LeftStickY => self.input_data.left.set_axis::<1>((-value).into()),
                Axis::RightStickX => self.input_data.right.set_axis::<0>(value.into()),
                Axis::RightStickY => self.input_data.right.set_axis::<1>((-value).into()),
                _ => (),
            },
            ButtonPressed(button, _) => {
                match button {
                    Button::South => {
                        let _ = self
                            .communicator
                            .send(InputMessage::Request(Request::StartSlowdown));
                    }
                    Button::Start => {
                        let _ = self
                            .communicator
                            .send(InputMessage::Request(Request::TogglePause));
                    }
                    Button::Select => {
                        let _ = self
                            .communicator
                            .send(InputMessage::Request(Request::Reload));
                    }
                    _ => (),
                }
                if !self.paused {
                    match button {
                        Button::LeftTrigger => self.input_data.left.set_grab(true),
                        Button::LeftTrigger2 => self.input_data.left.set_special(true),
                        Button::RightTrigger => self.input_data.right.set_grab(true),
                        Button::RightTrigger2 => self.input_data.right.set_special(true),
                        _ => (),
                    }
                }
            }
            ButtonReleased(button, _) => match button {
                Button::LeftTrigger => self.input_data.left.set_grab(false),
                Button::LeftTrigger2 => self.input_data.left.set_special(false),
                Button::RightTrigger => self.input_data.right.set_grab(false),
                Button::RightTrigger2 => self.input_data.right.set_special(false),
                _ => (),
            },
            _ => (),
        }
    }
}

impl<V: InnerSpace + Basis<0> + Basis<1>, C: Communicator<InputMessage<V>, WorldMessage<V>>> Update
    for Game<V, C>
where
    V::Scalar: From<f32> + Into<f32>,
{
    fn update(&mut self, timestep: f32) {
        if !self.initialized {
            while let Ok(received) = self.communicator.receive() {
                if let WorldMessage::Load(world) = received {
                    self.reload_world(world);
                    self.initialized = true;
                }
            }
            return;
        }

        while let Ok(received) = self.communicator.receive() {
            match received {
                WorldMessage::Load(world) => self.reload_world(world),
                WorldMessage::SetPaused(paused) => self.paused = paused,
                WorldMessage::SetSlowdown(slowdown) => self.slowdown = slowdown,
                WorldMessage::RefreshParameters {
                    group,
                    index,
                    parameters,
                } => {
                    if let Some(object) = &mut self.world.shape_groups[group].objects[index] {
                        object.parameters = parameters
                    }
                }
                WorldMessage::RefreshPoints {
                    group,
                    index,
                    points,
                } => {
                    if let Some(object) = &mut self.world.shape_groups[group].objects[index] {
                        object.points = points
                    }
                }
                WorldMessage::AddObject {
                    group,
                    index,
                    parameters,
                    points,
                    colors,
                } => self
                    .world
                    .add_object(group, index, parameters, points, colors),
                WorldMessage::RemoveObject { group, index } => {
                    self.world.remove_object(group, index)
                }
            }
        }

        while let Some(Event { event, .. }) = self.gilrs.next_event() {
            self.handle_gamepad_input(event);
        }

        if self.paused {
            return;
        }

        if self.input_data.mouse.changed() {
            let _ = self
                .communicator
                .send(InputMessage::Input(self.input_data.mouse));
        }
        if self.input_data.left.changed() {
            let _ = self
                .communicator
                .send(InputMessage::Input(self.input_data.left));
        }
        if self.input_data.right.changed() {
            let _ = self
                .communicator
                .send(InputMessage::Input(self.input_data.right));
        }

        self.update_view(timed_friction(self.follow_view, timestep));
    }
}

impl<
    V: InnerSpace + Basis<0> + Basis<1>,
    R: Renderer,
    C: Communicator<InputMessage<V>, WorldMessage<V>>,
> Render<Canvas<R>> for Game<V, C>
where
    V::Scalar: Into<f32> + From<f32>,
{
    fn render(&self, canvas: &mut Canvas<R>) {
        {
            let (w, h) = self.size;

            canvas.reset_transform();
            canvas.translate(w / 2.0, h / 2.0);
            let small_side = w.min(h);
            let zoom = small_side / self.view.zoom;
            canvas.scale(zoom, zoom);
            canvas.translate(-self.view.x, -self.view.y);
        }

        let (w, h) = (canvas.width() as f32, canvas.height() as f32);

        if self.slowdown > 0.0 {
            let mut background_path = Path::new();
            background_path.rect(-w / 2.0, -h / 2.0, w, h);

            let background_paint = Paint::color(
                self.world
                    .background_color
                    .a((2.0f32.powf(-self.slowdown) * 256.0) as u8)
                    .convert(),
            );

            canvas.fill_path(&background_path, &background_paint);
        } else {
            canvas.clear_rect(
                0,
                0,
                w as u32,
                h as u32,
                self.world.background_color.convert(),
            )
        }

        render_world(&self.world, canvas);

        canvas.flush();
    }
}

impl<
    V: InnerSpace + Basis<0> + Basis<1>,
    R: Renderer,
    C: Communicator<InputMessage<V>, WorldMessage<V>>,
> WindowInput<ModifiersState, Canvas<R>> for Game<V, C>
where
    V::Scalar: From<f32> + Into<f32>,
{
    fn input(
        &mut self,
        event: &WindowEvent<'_>,
        control_flow: &mut ControlFlow,
        modifiers: &mut ModifiersState,
        canvas: &mut Canvas<R>,
    ) {
        match event {
            &WindowEvent::ModifiersChanged(new_modifiers) => *modifiers = new_modifiers,

            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,

            WindowEvent::Resized(size) => {
                let (w, h) = (size.width, size.height);
                self.size = (w as f32, h as f32);
                canvas.set_size(w, h, 1.0);
            }

            WindowEvent::CursorMoved { position, .. } => {
                let (w, h) = self.size;

                let small_side = w.min(h);
                let zoom = small_side / self.view.zoom;

                let (x, y) = (
                    (position.x as f32 - self.size.0 / 2.0) / zoom + self.view.x,
                    (position.y as f32 - self.size.1 / 2.0) / zoom + self.view.y,
                );
                self.input_data
                    .mouse
                    .set_axes(V::bases_of::<0>(x.into()) + V::bases_of::<1>(y.into()));
            }

            &WindowEvent::Touch(Touch {
                location,
                phase,
                id,
                ..
            }) => {
                use TouchPhase::*;
                match phase {
                    Started => {
                        let (w, h) = self.size;

                        let small_side = w.min(h);
                        let zoom = small_side / self.view.zoom;

                        let (x, y) = (
                            (location.x as f32 - self.size.0 / 2.0) / zoom + self.view.x,
                            (location.y as f32 - self.size.1 / 2.0) / zoom + self.view.y,
                        );

                        let value = V::bases_of::<0>(x.into()) + V::bases_of::<1>(y.into());

                        if self.touch.first.is_none() {
                            self.input_data.left.set_axes(value);
                            self.input_data.left.set_grab(true);
                            self.touch.first = Some(id)
                        } else if self.touch.second.is_none() {
                            self.input_data.right.set_axes(value);
                            self.input_data.right.set_grab(true);
                            self.touch.second = Some(id)
                        }
                    }
                    Moved => {
                        let (w, h) = self.size;

                        let small_side = w.min(h);
                        let zoom = small_side / self.view.zoom;

                        let (x, y) = (
                            (location.x as f32 - self.size.0 / 2.0) / zoom + self.view.x,
                            (location.y as f32 - self.size.1 / 2.0) / zoom + self.view.y,
                        );

                        let value = V::bases_of::<0>(x.into()) + V::bases_of::<1>(y.into());

                        if Some(id) == self.touch.first {
                            self.input_data.left.set_axes(value)
                        }
                        if Some(id) == self.touch.second {
                            self.input_data.right.set_axes(value)
                        }
                    }
                    Ended | Cancelled => {
                        if Some(id) == self.touch.first {
                            self.touch.first = None;
                            self.input_data.left.set_grab(false)
                        }
                        if Some(id) == self.touch.second {
                            self.touch.second = None;
                            self.input_data.right.set_grab(false)
                        }
                    }
                }
            }

            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        virtual_keycode: Some(VirtualKeyCode::Return | VirtualKeyCode::Escape),
                        state: ElementState::Pressed,
                        ..
                    },
                ..
            } => {
                let _ = self
                    .communicator
                    .send(InputMessage::Request(Request::TogglePause));
            }

            WindowEvent::MouseInput { state, button, .. } => {
                use ElementState::*;
                use MouseButton::*;
                match (button, state) {
                    (Left, Pressed) => self.input_data.mouse.set_grab(true),
                    (Left, Released) => self.input_data.mouse.set_grab(false),
                    (Right, Pressed) => self.input_data.mouse.set_special(true),
                    (Right, Released) => self.input_data.mouse.set_special(false),
                    _ => (),
                }
            }

            _ => (),
        }
    }
}
